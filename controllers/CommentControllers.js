'use strict'

const Comment = require('../models/comment');

function getComments (req, res) {
  Comment.find({}, (err, comments) => {
    if (err) {
      return res.status(500).send({ message:`Error al realizar la peticion: ${err}` });
    }
    if (!comments) {
      return res.status(404).send({ message: `El comentario no existe`});
    }
    res.send(200, {comments});
  });
}

function getComment (req, res) {
  let commentId = req.params.artistId;

  Comment.find({ 'artistId': commentId }, (err, comment) => {
    if (err) {
      return res.status(500).send({ message:"Error al realizar la peticion" });
    }
    if (!comment) {
      return res.status(404).send({ message: `El comentario no existe`});
    }
    res.status(200).send({ comment });
  });
}

function saveComment(req, res){
  let comment = new Comment();
  comment.artistId = req.body.artistId;
  comment.comment = req.body.comment;
  comment.like = req.body.like;
  comment.date = req.body.date;

  comment.save((err, commentStored) => {
    if (err) res.status(500).send({ message: `Error al salvar en la base de datos: ${err}` });

    res.status(200).send({ comment: commentStored });
  });
}

function deleteComment (req, res) {
  let commentId = req.params.commentId
  Comment.findById(commentId, (err, comment) => {
    if (err) {
      return res.status(500).send({ message:"Error al realizar la peticion" });
    }
    if (!comment) {
      return res.status(404).send({ message: `El comentario no existe`});
    }
    comment.remove(err => {
      if (err) {
        res.status(500).send({ message:`Error al borrar el comentario: ${err}` });
      }
      res.status(200).send({ message: `El comentario ha sido eliminado satisfactoriamente`})
    });
  });
}

module.exports = {
    getComments,
    getComment,
    saveComment,
    deleteComment
}
