'use strict'

const Artist = require('../models/artist');

function getArtists (req, res) {
  Artist.find({}, (err, artists) => {
    if (err) {
      return res.status(500).send({ message:`Error al realizar la peticion: ${err}` });
    }
    if (!artists) {
      return res.status(404).send({ message: `El artista no existe`});
    }
    res.send(200, {artists});
  });
}

function getArtist (req, res) {
  let artistId = req.params.artistId;

  Artist.findById(artistId, (err, artist) => {
    if (err) {
      return res.status(500).send({ message:"Error al realizar la peticion" });
    }
    if (!artist) {
      return res.status(404).send({ message: `El artista no existe`});
    }
    res.status(200).send({ artist });
  });
}

function saveArtist(req, res){
  let artist = new Artist();
  artist.name = req.body.name;
  artist.song = req.body.song;
  artist.picture = req.body.picture;
  artist.cover = req.body.cover;
  artist.price = req.body.price;
  artist.preview = req.body.preview;
  artist.like = req.body.like;

  artist.save((err, artistStored) => {
    if (err) res.status(500).send({ message: `Error al salvar en la base de datos: ${err}` });

    res.status(200).send({ artist: artistStored });
  });
}

function updateArtist (req, res) {
  let artistId = req.params.artistId;
  let update = req.body;

  Artist.findByIdAndUpdate(artistId, update, (err, artistUpdated) => {
    if (err) {
      res.status(500).send({ message: `Error al actualizar el artista: ${err}`});
    }
    res.status(200).send({ artist: artistUpdated });
  });
}

module.exports = {
    getArtists,
    getArtist,
    saveArtist,
    updateArtist
}
