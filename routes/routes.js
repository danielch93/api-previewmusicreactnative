'use strict'

// const auth = require('../middlewares/auth');
// const ProductCtrl = require('../controllers/productControllers');
// const UserCtrl = require('../controllers/userControllers');

const express = require('express');
const ArtistCtrl = require('../controllers/ArtistControllers');
const CommentCtrl = require('../controllers/CommentControllers');
const api = express.Router();

// api.get('/product', auth, ProductCtrl.getProducts);
// api.post('/product', auth, ProductCtrl.saveProduct);
// api.put('/product/:productId', auth, ProductCtrl.updateProduct);
// api.delete('/product/:productId', auth, ProductCtrl.deleteProduct);

//Route Producst
// api.get('/product', ProductCtrl.getProducts);
// api.get('/product/:productId', ProductCtrl.getProduct);
// api.post('/product', ProductCtrl.saveProduct);
// api.put('/product/:productId', ProductCtrl.updateProduct);
// api.delete('/product/:productId', ProductCtrl.deleteProduct);
//
// api.post('/signup', UserCtrl.signUp);
// api.post('/signin', UserCtrl.signIn);
// api.get('/private', auth, function (req, res){
//     res.status(200).send({ message: 'Tienes acceso' });
// })

// Route Music
api.get('/artist', ArtistCtrl.getArtists);
api.post('/artist', ArtistCtrl.saveArtist);
api.get('/artist/:artistId', ArtistCtrl.getArtist);
api.put('/artist/:artistId', ArtistCtrl.updateArtist);

// Route Comment
api.get('/comment', CommentCtrl.getComments);
api.post('/comment', CommentCtrl.saveComment);
api.get('/comment/:artistId', CommentCtrl.getComment);
api.delete('/comment/:commentId', CommentCtrl.deleteComment);

module.exports = api;
