'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = Schema({
  artistId: {type: String},
  comment: {type: String, default: 'Sin comentarios'},
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Comment', CommentSchema);
