  'use strict'

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  const ArtistSchema = Schema({
    name: {type: String, default: 'Sin nombre'},
    song: {type: String, default: 'Sin cancion'},
    picture: {type: String, default: 'Sin imagen'},
    cover: {type: String, default: 'Sin cover'},
    price: {type: Number, default: 0},
    preview: {type: String, default: 'Sin audio'},
    like: {type: Number, default: 0},
  });

  module.exports = mongoose.model('Artist', ArtistSchema);
