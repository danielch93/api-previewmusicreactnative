var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var hotelsSchema = new Schema({
  name:         {type: String},

  price:        {type: Number},

  stars:        {type: Number},

  qualification:{type: String,
                  score:
                  [
                     'Muy recomendado',
                     'Recomendado',
                     'Bueno',
                     'No recomendado'
                   ]
                },

  description:  {type: String}
});

module.exports = mongoose.model('Hotels', hotelsSchema);
